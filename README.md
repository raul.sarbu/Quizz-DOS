# QuizzDOS
Quizzdos is a mobile application designed to provide an engaging and interactive experience for creating and answering quizzes. The application is built on a combination of modern technologies, including .Net 6.0 for the backend API, React Native with Expo for the frontend, and SSMS for the database.
## What sets Quizzdos apart?
What sets Quizzdos apart from other quiz apps is its focus on helping users retain information. The application features an innovative auto-generated "hint" function that makes use of ChatGPT's capabilities to provide users with a helpful prompt for certain questions. This feature saves time and effort by eliminating the need for manual input and encourages users to engage with the material in a more meaningful way.

## Installation steps:
1. Install LTS version of Node.JS https://nodejs.org/en
2. In a terminal: npm install -g expo-cli
3. In a terminal inside the quizzdos-frontend folder: npm install
4. In the same folder: npx expo start
5. In the backend folder, in a terminal dotnet run --urls "http://0.0.0.0:5000/" --project "[Path]"
Now from a mobile phone install "Expo Go" app and scan the QR from the frontend terminal or use an emulated device from Android Studio and press 'a' in the frontend terminal.

